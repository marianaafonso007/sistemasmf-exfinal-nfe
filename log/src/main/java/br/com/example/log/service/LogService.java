package br.com.example.log.service;

import br.com.example.log.enums.TipoSolicitacao;
import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

@Service
public class LogService {
    public void gravarLog(Solicitacao solicitacao, TipoSolicitacao tipoSolicitacao) {
        try {
            File arquivo = new File( "log.txt");
            StringBuilder sb = new StringBuilder();
            if (!arquivo.exists()) {
                arquivo.createNewFile();
            }else {
                sb.append('\n');
            }
            Date d = new Date();
            sb.append("[");
            sb.append(d);
            sb.append("] ");
            sb.append("[" + tipoSolicitacao +"]:");
            sb.append(solicitacao.getIdentidade());
            if(tipoSolicitacao == TipoSolicitacao.EMISSÃO){
                sb.append(" acaba de pedir a emissão de uma NF no valor de R$" + solicitacao.getValor() + "! ");
            }else {
                sb.append(" acaba de pedir os dados das suas notas fiscais.");
            }

            FileWriter fw = new FileWriter( arquivo, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(sb.toString());
            bw.close();fw.close();

            System.out.println("Log Salvo!");

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}

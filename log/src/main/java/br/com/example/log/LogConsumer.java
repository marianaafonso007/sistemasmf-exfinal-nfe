package br.com.example.log;

import br.com.example.log.enums.TipoSolicitacao;
import br.com.example.log.service.LogService;
import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LogConsumer {
    @Autowired
    private LogService logService;

    @KafkaListener(topics = "spec2-mariana-afonso-0", groupId = "teste3")
    public void receberEmissão(@Payload Solicitacao solicitacao) {
        logService.gravarLog(solicitacao, TipoSolicitacao.EMISSÃO);
    }

    @KafkaListener(topics = "spec2-mariana-afonso-1", groupId = "teste4")
    public void receberConsulta(@Payload Solicitacao solicitacao) {
        logService.gravarLog(solicitacao, TipoSolicitacao.CONSULTA);
    }
}

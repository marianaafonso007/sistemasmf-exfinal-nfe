package br.com.example.log.enums;

public enum TipoSolicitacao {
    EMISSÃO,
    CONSULTA
}

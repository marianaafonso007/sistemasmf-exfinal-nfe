package br.com.example.notafiscal.model;

public class NotaFiscal {
    private int idSolicitacao;
    private Double valorInicial;
    private Double valorIRRF;
    private Double valorCSLL;
    private Double valorCofins;

    public int getIdSolicitacao() {
        return idSolicitacao;
    }

    public void setIdSolicitacao(int idSolicitacao) {
        this.idSolicitacao = idSolicitacao;
    }

    public Double getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(Double valorInicial) {
        this.valorInicial = valorInicial;
    }

    public Double getValorIRRF() {
        return valorIRRF;
    }

    public void setValorIRRF(Double valorIRRF) {
        this.valorIRRF = valorIRRF;
    }

    public Double getValorCSLL() {
        return valorCSLL;
    }

    public void setValorCSLL(Double valorCSLL) {
        this.valorCSLL = valorCSLL;
    }

    public Double getValorCofins() {
        return valorCofins;
    }

    public void setValorCofins(Double valorCofins) {
        this.valorCofins = valorCofins;
    }
}

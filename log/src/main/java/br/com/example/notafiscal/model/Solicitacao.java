package br.com.example.notafiscal.model;

public class Solicitacao {
    private int id;
    private String identidade;
    private Double valor;
    private Boolean simplesNacional;
    private NotaFiscal nfe;

    public NotaFiscal getNfe() {
        return nfe;
    }

    public void setNfe(NotaFiscal nfe) {
        this.nfe = nfe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getSimplesNacional() {
        return simplesNacional;
    }

    public void setSimplesNacional(Boolean simplesNacional) {
        this.simplesNacional = simplesNacional;
    }

    public Solicitacao() {
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}

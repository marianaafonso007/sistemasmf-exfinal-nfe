package br.com.example.imposto.controller;

import br.com.example.imposto.ImpostoProducer;
import br.com.example.imposto.service.ImpostoService;
import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ImpostoController {
    @Autowired
    private ImpostoProducer impostoProducer;

    @Autowired
    private ImpostoService impostoService;

    @PostMapping
    public void finalizarCalculoImposto(@RequestBody Solicitacao solicitacao){
        NotaFiscal notaFiscal = impostoService.calculoImposto(solicitacao);
        solicitacao.setNfe(notaFiscal);
        impostoProducer.enviarAoKafka(solicitacao);
    }

}

package br.com.example.imposto;

import br.com.example.imposto.controller.ImpostoController;
import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ImpostoConsumer {
    @Autowired
    private ImpostoController impostoController;

    @KafkaListener(topics = "spec2-mariana-afonso-0", groupId = "teste1")
    public void receber(@Payload Solicitacao solicitacao) {
        impostoController.finalizarCalculoImposto(solicitacao);
    }

}

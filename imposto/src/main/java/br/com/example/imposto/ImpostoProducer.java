package br.com.example.imposto;

import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ImpostoProducer {
    @Autowired
    private KafkaTemplate<String, Solicitacao> producer;

    public void enviarAoKafka(Solicitacao solicitacao) {
        producer.send("spec2-mariana-afonso-2", solicitacao);
    }
}

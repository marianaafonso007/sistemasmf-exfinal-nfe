package br.com.example.imposto.service;

import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.stereotype.Service;

@Service
public class ImpostoService {

    public NotaFiscal calculoImposto(Solicitacao solicitacao){
        NotaFiscal notaFiscal = new NotaFiscal();
        notaFiscal.setIdSolicitacao(solicitacao.getId());
        notaFiscal.setValorInicial(solicitacao.getValor());
        notaFiscal.setValorIRRF(calculoIRFF(solicitacao.getValor()));
        if(!solicitacao.getSimplesNacional().booleanValue()){
            notaFiscal.setValorCSLL(calculoCSLL(solicitacao.getValor()));
            notaFiscal.setValorCofins(calculoPisCofis(solicitacao.getValor()));
        }else{
            notaFiscal.setValorCSLL(0.00);
            notaFiscal.setValorCofins(0.00);
        }
        return notaFiscal;
    }

    private Double calculoIRFF(Double valor){
        Double calculoIRFF = (valor * 1.5)/100;
        return  calculoIRFF;
    }
    private Double calculoCSLL(Double valor){
        Double calculoCSLL = (valor * 3)/100;
        return  calculoCSLL;
    }
    private Double calculoPisCofis(Double valor){
        Double calculoPisCofis = (valor * 0.65)/100;
        return  calculoPisCofis;
    }

}

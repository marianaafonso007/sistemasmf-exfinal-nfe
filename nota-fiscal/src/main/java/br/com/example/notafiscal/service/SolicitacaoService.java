package br.com.example.notafiscal.service;

import br.com.example.notafiscal.client.ConsultarCnpjClient;
import br.com.example.notafiscal.client.EmpresaDTO;
import br.com.example.notafiscal.exception.SolicitacaoNotFoundException;
import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.model.Solicitacao;
import br.com.example.notafiscal.model.enums.Status;
import br.com.example.notafiscal.repository.SolicitacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolicitacaoService {
    @Autowired
    private ConsultarCnpjClient consultarCnpjClient;

    @Autowired
    private NotaFiscalService notaFiscalService;

    @Autowired
    private SolicitacaoRepository solicitacaoRepository;

    public Solicitacao create(Solicitacao solicitacao){
        if(solicitacao.getIdentidade().length() < 14){
            solicitacao.setSimplesNacional(true);
        }else{
            EmpresaDTO validarCapital = consultarCnpjClient.getById(solicitacao.getIdentidade());
            if (Double.parseDouble(validarCapital.getCapital_social()) < 1000000){
                solicitacao.setSimplesNacional(true);
            }else{
                solicitacao.setSimplesNacional(false);
            }
        }
        return solicitacaoRepository.save(solicitacao);
    }

    public List<Solicitacao> getByIdentidade(String identidade){
        List<Solicitacao> solicitacaos = solicitacaoRepository.findByIdentidade(identidade);
        if (solicitacaos.size() > 0){
            return solicitacaos;
        }
        throw new SolicitacaoNotFoundException();
    }

    public void finalizacaoFluxo(Solicitacao solicitacao){
        Solicitacao solicitacao1 = solicitacaoRepository.findById(solicitacao.getId()).get();
        NotaFiscal nfe = solicitacao.getNfe();
        nfe.setValorInicial(solicitacao.getValor());
        NotaFiscal notaCriada = notaFiscalService.create(nfe);
        solicitacao1.setStatus(Status.complete);
        solicitacao1.setNfe(notaCriada);
        solicitacaoRepository.save(solicitacao1);
    }

}

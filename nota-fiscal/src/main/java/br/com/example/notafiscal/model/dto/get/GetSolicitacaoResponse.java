package br.com.example.notafiscal.model.dto.get;

import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.model.enums.Status;

public class GetSolicitacaoResponse {
    private String identidade;
    private Double valor;
    private Status status;
    private NotaFiscal nfe;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public NotaFiscal getNfe() {
        return nfe;
    }

    public void setNfe(NotaFiscal nfe) {
        this.nfe = nfe;
    }
}

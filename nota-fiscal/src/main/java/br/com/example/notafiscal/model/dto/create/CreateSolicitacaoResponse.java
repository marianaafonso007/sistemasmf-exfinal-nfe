package br.com.example.notafiscal.model.dto.create;

import br.com.example.notafiscal.model.enums.Status;

public class CreateSolicitacaoResponse {
    private Status status;

    public CreateSolicitacaoResponse(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}

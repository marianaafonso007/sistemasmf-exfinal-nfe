package br.com.example.notafiscal.repository;

import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SolicitacaoRepository extends CrudRepository<Solicitacao, Integer> {
    List<Solicitacao> findByIdentidade(String identidade);
}

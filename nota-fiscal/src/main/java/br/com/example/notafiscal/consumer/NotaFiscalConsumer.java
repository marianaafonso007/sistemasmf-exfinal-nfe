package br.com.example.notafiscal.consumer;

import br.com.example.notafiscal.model.Solicitacao;
import br.com.example.notafiscal.service.SolicitacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class NotaFiscalConsumer {

    @Autowired
    private SolicitacaoService solicitacaoService;

    @KafkaListener(topics = "spec2-mariana-afonso-2", groupId = "teste2")
    public void receber(@Payload Solicitacao solicitacao) {
        solicitacaoService.finalizacaoFluxo(solicitacao);
    }

}

package br.com.example.notafiscal.controller;

import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.model.Solicitacao;
import br.com.example.notafiscal.model.dto.create.CreateSolicitacaoRequest;
import br.com.example.notafiscal.model.dto.create.CreateSolicitacaoResponse;
import br.com.example.notafiscal.model.dto.get.GetSolicitacaoResponse;
import br.com.example.notafiscal.model.enums.Status;
import br.com.example.notafiscal.model.mapper.SolicitacaoMapper;
import br.com.example.notafiscal.producer.SolicitacaoProducer;
import br.com.example.notafiscal.service.NotaFiscalService;
import br.com.example.notafiscal.service.SolicitacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nfe")
public class NotaFiscalController {
    @Autowired
    private SolicitacaoProducer solicitacaoProducer;

    @Autowired
    private SolicitacaoService solicitacaoService;

    @Autowired
    private NotaFiscalService notaFiscalService;

    @PostMapping("/emitir")
    public CreateSolicitacaoResponse createSolicitacao(@RequestBody CreateSolicitacaoRequest createSolicitacaoRequest){
        Solicitacao solicitacao = SolicitacaoMapper.fromCreateRequest(createSolicitacaoRequest);
        Solicitacao solicitacaoCreate = solicitacaoService.create(solicitacao);
        solicitacaoProducer.enviarAoKafka(solicitacaoCreate);
        CreateSolicitacaoResponse createSolicitacaoResponse = new CreateSolicitacaoResponse(Status.pending);
        return createSolicitacaoResponse;
    }

    @GetMapping("/consultar/{cpf_cnpj}")
    public List<GetSolicitacaoResponse> getByIdentidade(@PathVariable String cpf_cnpj){
        solicitacaoProducer.enviarConsultaKafka(cpf_cnpj);
        List<Solicitacao> solicitacaos = solicitacaoService.getByIdentidade(cpf_cnpj);
        return SolicitacaoMapper.toGetResponse(solicitacaos);
    }

    @GetMapping("/consultar/")
    public Iterable<NotaFiscal> getALL(){
        return notaFiscalService.getAll();
    }

}

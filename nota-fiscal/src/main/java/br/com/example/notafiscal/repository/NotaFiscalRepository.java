package br.com.example.notafiscal.repository;

import br.com.example.notafiscal.model.NotaFiscal;
import org.springframework.data.repository.CrudRepository;

public interface NotaFiscalRepository extends CrudRepository<NotaFiscal, Long> {
}

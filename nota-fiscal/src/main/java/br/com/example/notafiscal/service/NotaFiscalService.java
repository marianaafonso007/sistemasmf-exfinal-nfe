package br.com.example.notafiscal.service;

import br.com.example.notafiscal.model.NotaFiscal;
import br.com.example.notafiscal.repository.NotaFiscalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotaFiscalService {

    @Autowired
    private NotaFiscalRepository notaFiscalRepository;

    public NotaFiscal create(NotaFiscal notaFiscal){
        Double valorFinal = notaFiscal.getValorInicial() - notaFiscal.getValorIRRF() - notaFiscal.getValorCSLL() - notaFiscal.getValorCofins();
        notaFiscal.setValorFinal(valorFinal);
        return notaFiscalRepository.save(notaFiscal);
    }

    public NotaFiscal findById(Long id){
        return notaFiscalRepository.findById(id).get();
    }

    public Iterable<NotaFiscal> getAll(){
        return notaFiscalRepository.findAll();
    }

}

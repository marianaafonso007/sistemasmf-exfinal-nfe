package br.com.example.notafiscal.model.mapper;

import br.com.example.notafiscal.model.Solicitacao;
import br.com.example.notafiscal.model.dto.create.CreateSolicitacaoRequest;
import br.com.example.notafiscal.model.dto.get.GetSolicitacaoResponse;
import br.com.example.notafiscal.model.enums.Status;

import java.util.ArrayList;
import java.util.List;

public class SolicitacaoMapper {

    public static Solicitacao fromCreateRequest(CreateSolicitacaoRequest createSolicitacaoRequest){
        Solicitacao solicitacao = new Solicitacao();
        solicitacao.setIdentidade(createSolicitacaoRequest.getIdentidade());
        solicitacao.setStatus(Status.pending);
        solicitacao.setValor(createSolicitacaoRequest.getValor());
        return solicitacao;
    }

    public static List<GetSolicitacaoResponse> toGetResponse(List<Solicitacao> solicitacaoList){
        List<GetSolicitacaoResponse> solicitacaoResponseList = new ArrayList<>();

        for (Solicitacao solicitacao : solicitacaoList){
            solicitacaoResponseList.add(toGetSolicitacaoResponse(solicitacao));
        }
        return solicitacaoResponseList;
    }

    public static GetSolicitacaoResponse toGetSolicitacaoResponse(Solicitacao solicitacao){
        GetSolicitacaoResponse getSolicitacaoResponse = new GetSolicitacaoResponse();
        getSolicitacaoResponse.setIdentidade(solicitacao.getIdentidade());
        getSolicitacaoResponse.setValor(solicitacao.getValor());
        getSolicitacaoResponse.setStatus(solicitacao.getStatus());
        if(solicitacao.getStatus() == Status.complete){
            getSolicitacaoResponse.setNfe(solicitacao.getNfe());
        }
        return getSolicitacaoResponse;
    }

}

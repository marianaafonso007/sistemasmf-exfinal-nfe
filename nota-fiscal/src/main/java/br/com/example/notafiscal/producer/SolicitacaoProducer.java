package br.com.example.notafiscal.producer;

import br.com.example.notafiscal.model.Solicitacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class SolicitacaoProducer {
    @Autowired
    private KafkaTemplate<String, Solicitacao> producer;

    public void enviarAoKafka(Solicitacao solicitacao) {
        producer.send("spec2-mariana-afonso-0", solicitacao);
    }

    public void enviarConsultaKafka(String identidade){
        Solicitacao solicitacao = new Solicitacao();
        solicitacao.setIdentidade(identidade);
        producer.send("spec2-mariana-afonso-1", solicitacao);
    }
}

package br.com.example.notafiscal.model.enums;

public enum Status {
    pending(1),
    complete(2);

    public int valor;

    public int getValor() {
        return valor;
    }
    Status(int valor){
        this.valor = valor;
    }
}
